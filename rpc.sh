#!/bin/bash

# RPC wrapper
# usage: GetBlockchainInfo
GetBlockchainInfo () {
    curl -s --user bitcoin:bitcoin --data-binary '{"jsonrpc": "1.0", "id":"", "method": "getblockchaininfo", "params": [] }' -H 'content-type: text/plain;' http://127.0.0.1:8332/
}


# RPC wrapper
# usage: GetBlockHeader <hash_or_height> <verbose>
GetBlockHeader () {
    curl -s --user bitcoin:bitcoin --data-binary '{"jsonrpc": "1.0", "id":"", "method": "getblockheader", "params": ['$1', '$2'] }' -H 'content-type: text/plain;' http://127.0.0.1:8332/
}

# RPC wrapper
# usage: GetBlockHash <height>
GetBlockHash () {
    curl -s --user bitcoin:bitcoin --data-binary '{"jsonrpc": "1.0", "id":"", "method": "getblockhash", "params": ['$1'] }' -H 'content-type: text/plain;' http://127.0.0.1:8332/
}

# RPC wrapper
# usage: GetBlockByHash <hash> <verbosity>
GetBlockByHash () {
    curl -s --user bitcoin:bitcoin --data-binary '{"jsonrpc": "1.0", "id":"", "method": "getblock", "params": ["'$1'", '$2'] }' -H 'content-type: text/plain;' http://127.0.0.1:8332/
}

# RPC wrapper
# usage: GetBlockByHeight <height> <verbosity>
GetBlockByHeight () {
    hashjson=$(GetBlockHash $1)
    jsonerror=$(echo $hashjson | jq -r .error)
    if [ "$jsonerror" = "null" ]
    then
        hash=$(echo $hashjson | jq -r .result)
        GetBlockByHash "$hash" "$2"
    else
        echo "$hashjson" > /dev/stderr
        return 1
    fi
}

# RPC wrapper
# usage: GetRawTransaction <txid> <verbose>
GetRawTransaction () {
    curl -s --user bitcoin:bitcoin --data-binary '{"jsonrpc": "1.0", "id":"", "method": "getrawtransaction", "params": ["'$1'", '$2'] }' -H 'content-type: text/plain;' http://127.0.0.1:8332/
}
