#!/bin/bash

height_start=$1
height_end=$2
query_blocks=$3
database_file_name=$4
table_name=$5

source rpc.sh
export -f GetBlockHeader

anyHedgePattern=5e79519c637b69517a7cad517a7cad6d6d6d6d6d755167517a519dc3519d517a51795179bb517a5179517abb5179517f77517f75817651a0695179517f77517f75818c9d517a517f758151799f695179517f77817651a0695179a35179a4517a517f758176517aa269517aa278517a8b517aa5919b695176517a517a96517a94a47c517a517994a4c4519d51cc7b9d51cd517a8851cc9d51cd517a8777777768

# args: database_file_name, table_name
function csv_to_sqlite() {
  sqlite3 -csv "$1" ".import '|cat -' $2"
}

function get_timestamp() {
    GetBlockHeader $1 true | jq '.result.time'
}
export -f get_timestamp

# args: unlocking_bytecode
# output: csv of AnyHedge input script data (redeem script omitted)
function get_input_script_field() {
    local pushes=$(echo "$1" | xxd -r -p | ./parse_pushes -d)
    readarray <<<"$pushes" pushes
    if [ ${#pushes[@]} -eq 6 ]
    then
        pushes=( "${pushes[@]:4:1}" "${pushes[@]:0:4}" "" "" )
        echo -n "${pushes[@]:0:7}" | tr ' ' ',' | tr -d '\n'
    elif [ ${#pushes[@]} -eq 4 ]
    then
        pushes=( "${pushes[@]:2:1}" "" "" "" "" "${pushes[@]:0:2}" )
        echo -n "${pushes[@]:0:7}" | tr ' ' ',' | tr -d '\n'
    fi
}
export -f get_input_script_field

# args: unlocking_bytecode
# output: csv of AnyHedge redeem script parameters
function get_redeem_script_field() {
    local pushes=$(echo "$1" | xxd -r -p | ./parse_pushes | tail -n 1 | cut -b 3- | xxd -r -p | ./parse_pushes -d)
    readarray <<<"$pushes" pushes
    echo -n "${pushes[@]:0:13}" | tr ' ' ',' | tr -d '\n'
}
export -f get_redeem_script_field

# args: oracle_message
# output: csv of the 4 oracle price message fields
function parse_oracle_message() {
    local ofields=(\
        $(echo "$1" | cut -c 3-10 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 11-18 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 19-26 | xxd -r -p | ./bin2scriptnum 4)\
        $(echo "$1" | cut -c 27-34 | xxd -r -p | ./bin2scriptnum 4)\
    )
    echo -n "${ofields[@]}" | tr ' ' ',' | tr -d '\n'
}
export -f parse_oracle_message

height=$height_start
while [ $height -le $height_end ]
do
    echo "query block $height" >> /dev/stderr
    result=$((GetBlockByHeight $height 3 | ./rpc_extra_injector | jq '[.result.tx[] | select(.vin[0].scriptSig.redeemPatternHex == "'$anyHedgePattern'")]') 2>&1)
    if [ "${result:10:4}" = "null" ]
    then
        echo "$result" | jq >> /dev/stderr
        exit 1
    fi
    timestamp=$(GetBlockHeader $height true | jq '.result.time')
    rowcount=$(echo "$result" | jq -r 'map(.txid) | length')
    echo $rowcount' rows' >> /dev/stderr
    echo "$result" | jq --arg height $height --arg timestamp $timestamp -r 'map(["\\x"+.vin[0].txid, .vin[0].vout, (.vin[0].prevout.value*100000000 | round), .vin[0].prevout.scriptPubKey.hex, "pattern", .vin[0].prevout.height, "timestamp", $height, $timestamp, .vin[0].scriptSig.hex, .vin[0].scriptSig.inputPatternHex, .vin[0].scriptSig.redeemPatternHex, .vin[0].prevout.scriptPubKey.address] | join(",")) | join("\n")' \
    | awk -F "," '$3!=""' \
    | awk -F "," '{OFS=","; "get_input_script_field " $10 | getline rp1; close("get_input_script_field " $10); "get_redeem_script_field " $10 | getline rp2; close("get_redeem_script_field " $10); "get_timestamp " $6 | getline rp3; close("get_timestamp " $6); print $1,$2,$13,$3,$6,rp3,$8,$9,rp1,rp2}' \
    | awk -F "," '{OFS=","; if ($(NF-19)==1) {m1=$(NF-17); m2=$(NF-15); "parse_oracle_message " m1 | getline o1; close("parse_oracle_message " m1); "parse_oracle_message " m2 | getline o2; close("parse_oracle_message " m2);} else {o1=",,,"; o2=",,,"} print $0,o1,o2}' \
    | csv_to_sqlite "$database_file_name" "$table_name" 2>&1 | grep -v 'UNIQUE constraint failed' 1>&2
    height=$((height+1))
    echo $height > .height
done
