startheight=$(cat .height)
endheight=$(./get_current_height.sh)
endheight=$((endheight-2))
if [ -z $startheight ]
then
    startheight=743774
    ./pull_data_v011.sh $startheight $endheight 1 anyhedge_stats.sqlite anyhedge_closed_contracts_v011
    startheight=819578
    ./pull_data_v012.sh $startheight $endheight 1 anyhedge_stats.sqlite anyhedge_closed_contracts_v012
else
    ./pull_data_v011.sh $startheight $endheight 1 anyhedge_stats.sqlite anyhedge_closed_contracts_v011
    ./pull_data_v012.sh $startheight $endheight 1 anyhedge_stats.sqlite anyhedge_closed_contracts_v012
fi
./export_closed_contracts.sh
./export_oracle.sh 
./export_stats_daily.sh 
./plot_stats_daily.sh
./split_stats_daily.sh
