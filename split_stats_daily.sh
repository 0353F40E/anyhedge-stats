#!/bin/bash
mkdir -p stats_daily
file=stats_daily.csv
header=$(awk 'NR==1' $file)
awk -F',' '$1!=prev{close(out); out="stats_daily/"$1".csv"; prev=$1} {print "'"$header"'" > out; print > out}' "$file"
rm stats_daily/day.csv
