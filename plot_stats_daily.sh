#!/usr/bin/gnuplot
set lt 1 lc rgb 'red'
set lt 2 lc rgb 'dark-blue'
set lt 3 lc rgb 'green'
set lt 4 lc rgb 'orange-red'
set lt 5 lc rgb 'blue'
set lt 6 lc rgb 'orange'
set lt 7 lc rgb 'yellow'
set lt 8 lc rgb 'violet'
firstcol=2
cumulated(i)=((i>firstcol)?column(i)+cumulated(i-1):(i==firstcol)?column(i):1/0)
set datafile separator ','
set decimalsign locale
set format y "%'.0f"
set xdata time
set xlabel "Date"
set ylabel "BCH"
set timefmt "%Y-%m-%d"
set format x "%Y-%m"
set key autotitle columnhead noenhanced
set key left top vertical Left
set xtics out rotate
set xtics 2629746
set xrange ["2022-10-01":*]
set term png size 640, 480
# Volume
set title "Closed Contract's Volume"
set output "plots/volume_closed.png"
plot 'stats_daily.csv' using 1:6 with lines
set output "plots/volume_closed_by_pair.png"
plot for [col=2:9] "pivot_daily_volume_closed.csv" using 1:(cumulated(11-col)) title columnheader(11-col) with filledcurves x1
set title "Closed Contract's Cumulative Volume"
set output "plots/volume_closed_cumulative.png"
plot 'stats_daily.csv' using 1:7 with lines
set output "plots/volume_closed_cumulative_by_pair.png"
plot for [col=2:9] "pivot_daily_volume_closed_cumulative.csv" using 1:(cumulated(11-col)) title columnheader(11-col) with filledcurves x1
# TVL
set title "Total Value Locked (TVL)"
set output "plots/tvl.png"
plot 'stats_daily.csv' using 1:11 with lines
set output "plots/volume_open_closed_cumulative.png"
plot 'stats_daily.csv' using 1:3 with lines,'stats_daily.csv' using 1:7 with lines
set output "plots/tvl_by_pair.png"
plot for [col=2:9] "pivot_daily_tvl.csv" using 1:(cumulated(11-col)) title columnheader(11-col) with filledcurves x1
