BEGIN TRANSACTION;
CREATE TABLE IF NOT EXISTS "anyhedge_oracles" (
	"pubkey"	TEXT,
	"pair_description"	TEXT,
	"pair_ticker"	TEXT,
	"operator"	TEXT,
	"period"	TEXT,
	PRIMARY KEY("pubkey")
);
CREATE TABLE IF NOT EXISTS "anyhedge_closed_contracts_v011" (
	"outpoint_transaction_hash"	TEXT,
	"outpoint_index"	INTEGER,
	"contract_address"	TEXT,
	"outpoint_value_satoshis"	INTEGER,
	"open_block_height"	INTEGER,
	"open_block_timestamp"	INTEGER,
	"close_block_height"	INTEGER,
	"close_block_timestamp"	INTEGER,
	"is_payout"	INTEGER,
	"previous_message_signature"	TEXT,
	"previous_message"	TEXT,
	"settlement_message_signature"	TEXT,
	"settlement_message"	TEXT,
	"hedge_mutual_redeem_signature"	TEXT,
	"long_mutual_redeem_signature"	TEXT,
	"maturity_timestamp"	INTEGER,
	"start_timestamp"	INTEGER,
	"high_liquidation_price"	INTEGER,
	"low_liquidation_price"	INTEGER,
	"payout_sats"	INTEGER,
	"nominal_units_x_sats_per_bch"	INTEGER,
	"oracle_public_key"	TEXT,
	"long_lock_script"	TEXT,
	"hedge_lock_script"	TEXT,
	"enable_mutual_redemption"	INTEGER,
	"long_mutual_redeem_public_key"	TEXT,
	"hedge_mutual_redeem_public_key"	TEXT,
	"previous_message_timestamp"	INTEGER,
	"previous_message_m_sequence"	INTEGER,
	"previous_message_p_sequence"	INTEGER,
	"previous_message_price"	INTEGER,
	"settlement_message_timestamp"	INTEGER,
	"settlement_message_m_sequence"	INTEGER,
	"settlement_message_p_sequence"	INTEGER,
	"settlement_message_price"	INTEGER,
	PRIMARY KEY("outpoint_transaction_hash","outpoint_index")
);
CREATE TABLE IF NOT EXISTS "anyhedge_closed_contracts_v012" (
	"outpoint_transaction_hash"	TEXT,
	"outpoint_index"	INTEGER,
	"contract_address"	TEXT,
	"outpoint_value_satoshis"	INTEGER,
	"open_block_height"	INTEGER,
	"open_block_timestamp"	INTEGER,
	"close_block_height"	INTEGER,
	"close_block_timestamp"	INTEGER,
	"is_payout"	INTEGER,
	"previous_message_signature"	TEXT,
	"previous_message"	TEXT,
	"settlement_message_signature"	TEXT,
	"settlement_message"	TEXT,
	"hedge_mutual_redeem_signature"	TEXT,
	"long_mutual_redeem_signature"	TEXT,
	"maturity_timestamp"	INTEGER,
	"start_timestamp"	INTEGER,
	"high_liquidation_price"	INTEGER,
	"low_liquidation_price"	INTEGER,
	"payout_sats"	INTEGER,
	"sats_for_nominal_units_at_high_liquidation"	INTEGER,
	"nominal_units_x_sats_per_bch"	INTEGER,
	"oracle_public_key"	TEXT,
	"long_lock_script"	TEXT,
	"hedge_lock_script"	TEXT,
	"enable_mutual_redemption"	INTEGER,
	"long_mutual_redeem_public_key"	TEXT,
	"hedge_mutual_redeem_public_key"	TEXT,
	"previous_message_timestamp"	INTEGER,
	"previous_message_m_sequence"	INTEGER,
	"previous_message_p_sequence"	INTEGER,
	"previous_message_price"	INTEGER,
	"settlement_message_timestamp"	INTEGER,
	"settlement_message_m_sequence"	INTEGER,
	"settlement_message_p_sequence"	INTEGER,
	"settlement_message_price"	INTEGER,
	PRIMARY KEY("outpoint_transaction_hash","outpoint_index")
);
CREATE VIEW "dayrange" AS 
WITH RECURSIVE dates(date) AS (
  VALUES((select date(min(close_block_timestamp), 'unixepoch') FROM "anyhedge_closed_contracts"))
  UNION ALL
  SELECT date(date, '+1 day')
  FROM dates
  WHERE date < (select date(max(close_block_timestamp), 'unixepoch') FROM "anyhedge_closed_contracts")
)
SELECT date AS day FROM dates;
CREATE VIEW "stats_daily_by_pair" AS
SELECT
    day,
	pair_ticker,
    volume_opened/1e8 as volume_opened,
    volume_opened_cumulative/1e8 as volume_opened_cumulative,
    count_opened,
    count_opened_cumulative,
    volume_closed/1e8 as volume_closed,
    volume_closed_cumulative/1e8 as volume_closed_cumulative,
    count_closed,
    count_closed_cumulative,
    tvl_change/1e8 as tvl_change,
    tvl/1e8 as tvl
FROM
(
SELECT
    day,
	pair_ticker,
    volume_opened as volume_opened,
    SUM(volume_opened) OVER (
		PARTITION BY pair_ticker
        ORDER BY day, pair_ticker
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS volume_opened_cumulative,
    count_opened,
    SUM(count_opened) OVER (
		PARTITION BY pair_ticker
        ORDER BY day, pair_ticker
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS count_opened_cumulative,
    volume_closed as volume_closed,
    SUM(volume_closed) OVER (
		PARTITION BY pair_ticker
        ORDER BY day, pair_ticker
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS volume_closed_cumulative,
    count_closed,
    SUM(count_closed) OVER (
		PARTITION BY pair_ticker
        ORDER BY day, pair_ticker
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS count_closed_cumulative,
    tvl_change as tvl_change,
    SUM(tvl_change) OVER (
		PARTITION BY pair_ticker
        ORDER BY day, pair_ticker
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS tvl
FROM
(
SELECT 
  t1.day as day, 
  t1.pt as pair_ticker,
  sum(iif(volume_opened is NULL, 0, volume_opened)) as volume_opened,
  sum(iif(count_opened is NULL, 0, count_opened)) as count_opened,
  sum(iif(volume_closed is NULL, 0, volume_closed)) as volume_closed,
  sum(iif(count_closed is NULL, 0, count_closed)) as count_closed,
  sum(iif(tvl_change is NULL, 0, tvl_change)) as tvl_change
FROM
  (SELECT day, pt FROM dayrange JOIN (SELECT DISTINCT anyhedge_oracles.pair_ticker AS pt FROM anyhedge_oracles)) AS t1 LEFT JOIN
  (
    SELECT 
      day,
	  pair_ticker,
      sum(volume_opened) as volume_opened,
      sum(count_opened) as count_opened,
      sum(volume_closed) as volume_closed,
      sum(count_closed) as count_closed,
      sum(tvl_change) as tvl_change
    FROM 
      (
        SELECT 
          date(open_block_timestamp, 'unixepoch') AS day,
		  pair_ticker,
          sum(outpoint_value_satoshis) AS volume_opened,
          0 AS volume_closed,
          count(outpoint_value_satoshis) AS count_opened,
          0 AS count_closed,
          sum(outpoint_value_satoshis) AS tvl_change
        FROM 
          "anyhedge_closed_contracts" INNER JOIN anyhedge_oracles ON "anyhedge_closed_contracts".oracle_public_key == anyhedge_oracles.pubkey
        GROUP BY 
          date(open_block_timestamp, 'unixepoch'), pair_ticker, volume_closed
        UNION ALL 
        SELECT 
          date(close_block_timestamp, 'unixepoch') AS day,
		  pair_ticker,
          0 AS volume_opened,
          sum(outpoint_value_satoshis) AS volume_closed,
          0 AS count_opened,
          count(outpoint_value_satoshis) AS count_closed,
          -sum(outpoint_value_satoshis) AS tvl_change
        FROM 
          "anyhedge_closed_contracts" INNER JOIN anyhedge_oracles ON "anyhedge_closed_contracts".oracle_public_key == anyhedge_oracles.pubkey
        GROUP BY 
          date(close_block_timestamp, 'unixepoch'), pair_ticker, volume_opened
      ) 
    GROUP BY 
      day, pair_ticker
  ) as t2
  ON (t1.day == t2.day AND t1.pt == t2.pair_ticker)
GROUP BY 
  t1.day, t1.pt
ORDER BY
  t1.day, pair_ticker
) AS daily
ORDER BY day, pair_ticker
);
CREATE VIEW "pivot_daily_volume_closed_cumulative" AS SELECT
	day,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHUSD') as BCHUSD,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHCNY') as BCHCNY,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHINR') as BCHINR,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHXAU') as BCHXAU,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHXAG') as BCHXAG,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHBTC') as BCHBTC,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHETH') as BCHETH,
	sum(volume_closed_cumulative) FILTER (WHERE pair_ticker == 'BCHDOGE') as BCHDOGE
FROM stats_daily_by_pair
GROUP BY day
ORDER BY day;
CREATE VIEW "pivot_daily_volume_closed" AS SELECT
	day,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHUSD') as BCHUSD,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHCNY') as BCHCNY,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHINR') as BCHINR,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHXAU') as BCHXAU,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHXAG') as BCHXAG,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHBTC') as BCHBTC,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHETH') as BCHETH,
	sum(volume_closed) FILTER (WHERE pair_ticker == 'BCHDOGE') as BCHDOGE
FROM stats_daily_by_pair
GROUP BY day
ORDER BY day;
CREATE VIEW "pivot_daily_tvl" AS 
SELECT
	day,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHUSD') as BCHUSD,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHCNY') as BCHCNY,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHINR') as BCHINR,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHXAU') as BCHXAU,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHXAG') as BCHXAG,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHBTC') as BCHBTC,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHETH') as BCHETH,
	sum(tvl) FILTER (WHERE pair_ticker == 'BCHDOGE') as BCHDOGE
FROM stats_daily_by_pair
GROUP BY day
ORDER BY day;
CREATE VIEW "liquidations_daily_by_pair" AS 
SELECT
  date(close_block_timestamp, 'unixepoch') as day,
  anyhedge_oracles.pair_ticker as pair,
  iif(settlement_message_price <= low_liquidation_price, 'long', 'hedge') as liquidated_side,
  count(close_block_timestamp) as count,
  sum(outpoint_value_satoshis)/1e8 as liquidated_value
FROM "anyhedge_closed_contracts" LEFT JOIN anyhedge_oracles ON "anyhedge_closed_contracts".oracle_public_key == anyhedge_oracles.pubkey
WHERE (settlement_message_price != '') AND (settlement_message_price <= low_liquidation_price OR settlement_message_price >= high_liquidation_price)
GROUP BY day, pair, liquidated_side;
CREATE VIEW "liquidations_daily" AS 
SELECT
  date(close_block_timestamp, 'unixepoch') as day,
  iif(settlement_message_price <= low_liquidation_price, 'long', 'hedge') as liquidated_side,
  count(close_block_timestamp) as count,
  sum(outpoint_value_satoshis)/1e8 as liquidated_value
FROM "anyhedge_closed_contracts"
WHERE (settlement_message_price != '') AND (settlement_message_price <= low_liquidation_price OR settlement_message_price >= high_liquidation_price)
GROUP BY day, liquidated_side;
CREATE VIEW "stats_daily" AS
SELECT
    day,
    volume_opened/1e8 as volume_opened,
    volume_opened_cumulative/1e8 as volume_opened_cumulative,
    count_opened,
    count_opened_cumulative,
    volume_closed/1e8 as volume_closed,
    volume_closed_cumulative/1e8 as volume_closed_cumulative,
    count_closed,
    count_closed_cumulative,
    tvl_change/1e8 as tvl_change,
    tvl/1e8 as tvl,
	count_active as count_active,
	iif(earliest_open_day IS NULL, day, earliest_open_day) as earliest_open_day
FROM
(
SELECT
    day,
    volume_opened as volume_opened,
    SUM(volume_opened) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS volume_opened_cumulative,
    count_opened,
    SUM(count_opened) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS count_opened_cumulative,
    volume_closed as volume_closed,
    SUM(volume_closed) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS volume_closed_cumulative,
    count_closed,
    SUM(count_closed) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS count_closed_cumulative,
    tvl_change as tvl_change,
    SUM(tvl_change) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS tvl,
	count_change as count_change,
    SUM(count_change) OVER (
        ORDER BY day
        ROWS BETWEEN
            UNBOUNDED PRECEDING
            AND CURRENT ROW
        ) AS count_active,
	earliest_open_day
FROM
(
SELECT 
  t1.day as day, 
  sum(iif(volume_opened is NULL, 0, volume_opened)) as volume_opened,
  sum(iif(count_opened is NULL, 0, count_opened)) as count_opened,
  sum(iif(volume_closed is NULL, 0, volume_closed)) as volume_closed,
  sum(iif(count_closed is NULL, 0, count_closed)) as count_closed,
  sum(iif(tvl_change is NULL, 0, tvl_change)) as tvl_change,
  sum(iif(count_change is NULL, 0, count_change)) as count_change,
  min(earliest_open_day) AS earliest_open_day
FROM
  dayrange AS t1 LEFT JOIN
  (
    SELECT 
      day,
      sum(volume_opened) as volume_opened,
      sum(count_opened) as count_opened,
      sum(volume_closed) as volume_closed,
      sum(count_closed) as count_closed,
      sum(tvl_change) as tvl_change,
	  sum(count_change) as count_change,
	  min(earliest_open_day) AS earliest_open_day
    FROM 
      (
        SELECT 
          date(open_block_timestamp, 'unixepoch') AS day,
          sum(outpoint_value_satoshis) AS volume_opened,
          0 AS volume_closed,
          count(outpoint_value_satoshis) AS count_opened,
          0 AS count_closed,
          sum(outpoint_value_satoshis) AS tvl_change,
		  count(outpoint_value_satoshis) AS count_change,
		  min(date(open_block_timestamp, 'unixepoch')) AS earliest_open_day
        FROM 
          "anyhedge_closed_contracts" 
        GROUP BY 
          date(open_block_timestamp, 'unixepoch'), volume_closed 
        UNION ALL 
        SELECT 
          date(close_block_timestamp, 'unixepoch') AS day,
          0 AS volume_opened,
          sum(outpoint_value_satoshis) AS volume_closed,
          0 AS count_opened,
          count(outpoint_value_satoshis) AS count_closed,
          -sum(outpoint_value_satoshis) AS tvl_change,
		  -count(outpoint_value_satoshis) AS count_change,
		  min(date(open_block_timestamp, 'unixepoch')) AS earliest_open_day
        FROM 
          "anyhedge_closed_contracts" 
        GROUP BY 
          date(close_block_timestamp, 'unixepoch'), volume_opened
      ) 
    GROUP BY 
      day
  ) as t2
  ON t1.day == t2.day
GROUP BY 
  t1.day
ORDER BY
  t1.day
) AS daily
ORDER BY day
);
CREATE VIEW "anyhedge_closed_contracts" AS SELECT
	"outpoint_transaction_hash",
	"outpoint_index",
	"contract_address",
	"outpoint_value_satoshis",
	"open_block_height",
	"open_block_timestamp",
	"close_block_height",
	"close_block_timestamp",
	"is_payout",
	"previous_message_signature",
	"previous_message",
	"settlement_message_signature",
	"settlement_message",
	"hedge_mutual_redeem_signature",
	"long_mutual_redeem_signature",
	"maturity_timestamp",
	"start_timestamp",
	"high_liquidation_price",
	"low_liquidation_price",
	"payout_sats",
	"sats_for_nominal_units_at_high_liquidation",
	"nominal_units_x_sats_per_bch",
	"oracle_public_key",
	"long_lock_script",
	"hedge_lock_script",
	"enable_mutual_redemption",
	"long_mutual_redeem_public_key",
	"hedge_mutual_redeem_public_key",
	"previous_message_timestamp",
	"previous_message_m_sequence",
	"previous_message_p_sequence",
	"previous_message_price",
	"settlement_message_timestamp",
	"settlement_message_m_sequence",
	"settlement_message_p_sequence",
	"settlement_message_price",
	0.12 contract_version
FROM
"anyhedge_closed_contracts_v012"

UNION ALL

SELECT
	"outpoint_transaction_hash",
	"outpoint_index",
	"contract_address",
	"outpoint_value_satoshis",
	"open_block_height",
	"open_block_timestamp",
	"close_block_height",
	"close_block_timestamp",
	"is_payout",
	"previous_message_signature",
	"previous_message",
	"settlement_message_signature",
	"settlement_message",
	"hedge_mutual_redeem_signature",
	"long_mutual_redeem_signature",
	"maturity_timestamp",
	"start_timestamp",
	"high_liquidation_price",
	"low_liquidation_price",
	"payout_sats",
	0 sats_for_nominal_units_at_high_liquidation,
	"nominal_units_x_sats_per_bch",
	"oracle_public_key",
	"long_lock_script",
	"hedge_lock_script",
	"enable_mutual_redemption",
	"long_mutual_redeem_public_key",
	"hedge_mutual_redeem_public_key",
	"previous_message_timestamp",
	"previous_message_m_sequence",
	"previous_message_p_sequence",
	"previous_message_price",
	"settlement_message_timestamp",
	"settlement_message_m_sequence",
	"settlement_message_p_sequence",
	"settlement_message_price",
	0.11 contract_version
FROM
"anyhedge_closed_contracts_v011";
COMMIT;
