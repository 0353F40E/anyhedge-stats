### Closed Contracts Volume

#### Daily Volume Total

<img src=volume_closed.png></img>

#### Daily Volume By Pair

<img src=volume_closed_by_pair.png></img>

#### Cumulative Volume Total

<img src=volume_closed_cumulative.png></img>

#### Cumulative Volume By Pair

<img src=volume_closed_cumulative_by_pair.png></img>

### Opened And Closed Cumulative Volumes

Note that the opened contracts volume does not account for currenctly active contracts, which is due to AnyHedge contracts being secret until settled.
The difference between the `volume_opened_cumulative` and `volume_closed_cumulative` will give us the Total Value Locked (plotted below), and it will always add up to 0 at current time due to opened-not-yet-closed contracts being invisible.
This also means that historical data will be back-filled every time a contract is revealed.

<img src=volume_open_closed_cumulative.png></img>

### Total Value Locked (TVL)

#### TVL Total

<img src=tvl.png></img>

#### TVL By Pair

<img src=tvl_by_pair.png></img>