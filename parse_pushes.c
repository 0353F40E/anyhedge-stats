#include <stdio.h>
#include <stdint.h>
#include <string.h>

int parse_bytes(size_t len, int decode_numbers)
{
    int c;
    if (len > 0x08u || !decode_numbers) {
        printf("0x");
        while (len-- > 0) {
            c = getchar();
            if (c == EOF)
                return 1;
            printf("%02x", c);
        }
    }
    else {
        uint64_t number = 0;
        for (int i = 0; i < len*8; i+=8) {
            c = getchar();
            if (c == EOF)
                return 1;
            number += (uint64_t) c << i;
        }
        uint64_t sign = number & (1ull << (len*8-1));
        if (sign)
            printf("%ld", (int64_t) 0 - (number & ~sign));
        else
            printf("%lu", number);
    }
    putchar('\n');
    return 0;
}

int main(int argc, char *argv[])
{
    int decode_numbers = 0;
    if (argc > 1 && !strcmp(argv[1], "-d"))
        decode_numbers = 1;
    int c = getchar();
    while (c != EOF) {
        uint8_t uc = (uint8_t) c;
        if (uc <= 0x60u) {
            if (uc == 0x00u) {
                if (decode_numbers)
                    printf("0\n");
                else
                    printf("0x00\n");
            }
            else if (uc >= 0x51u) {
                if (decode_numbers)
                    printf("%d\n", (int8_t) uc - 0x50u);
                else
                    printf("0x%02x\n", uc - 0x50u);
            }
          //else if (uc == 0x50u)
          //    ;
            else if (uc == 0x4fu) {
                if (decode_numbers)
                    printf("-1\n");
                else
                    printf("0x4f\n");
            }
            else if (uc <= 0x4bu) {
                if (parse_bytes(uc, decode_numbers))
                    return 1;
            }
            else if (uc == 0x4cu) {
                c = getchar();
                if (c == EOF)
                    return 1;
                if (parse_bytes((uint8_t) c, decode_numbers))
                    return 1;
            }
            else if (uc == 0x4du) {
                uint16_t u2 = 0;
                for (int i = 0; i < 16; i+=8) {
                    c = getchar();
                    if (c == EOF)
                        return 1;
                    u2 += (uint16_t) c << i;
                }
                if (parse_bytes(u2, decode_numbers))
                    return 1;
            }
            else if (uc == 0x4eu) {
                uint32_t u4 = 0;
                for (int i = 0; i < 32; i+=8) {
                    c = getchar();
                    if (c == EOF)
                        return 1;
                    u4 += (uint32_t) c << i;
                }
                if (parse_bytes(u4, decode_numbers));
                    return 1;
            }
        }
        c = getchar();
    }
    return 0;
}
