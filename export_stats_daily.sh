sqlite3 -header -csv anyhedge_stats.sqlite "select * from stats_daily;" > stats_daily.csv
sqlite3 -header -csv anyhedge_stats.sqlite "select * from stats_daily_by_pair;" > stats_daily_by_pair.csv
sqlite3 -header -csv anyhedge_stats.sqlite "select * from pivot_daily_volume_closed;" > pivot_daily_volume_closed.csv
sqlite3 -header -csv anyhedge_stats.sqlite "select * from pivot_daily_volume_closed_cumulative;" > pivot_daily_volume_closed_cumulative.csv
sqlite3 -header -csv anyhedge_stats.sqlite "select * from pivot_daily_tvl;" > pivot_daily_tvl.csv
